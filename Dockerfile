FROM haskell:7.8
MAINTAINER Jan-Philip Loos <jloos@maxdaten.io>

# install build utils
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      make


RUN mkdir -p /haskell-scratch/images
WORKDIR /haskell-scratch/
COPY container.Makefile /haskell-scratch/Makefile

CMD make
