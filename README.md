# haskell-scratch

Base Docker builder and minimal image which includes minimal shared libraries for GHC-compiled executables. Based on [haskell scratch](https://www.fpcomplete.com/blog/2015/05/haskell-web-server-in-5mb).
It could be to minimal, especially if you need system infrastructure (DNS lookups).

## Prerequisites

- docker

## Usage

`make`

## What it does

1. creates an environment image based on `haskell` image
2. builds `haskell-scratch` image tars in a from host mounted volume
3. locally imports images
4. pushes images to quay.io