IMAGE_DIR = images

default: docker-integer-gmp docker-integer-simple

image:
	@docker build -t haskell-run-env:builder .

docker-integer-gmp: | image
	@docker run -v $(CURDIR)/$(IMAGE_DIR):/haskell-scratch/$(IMAGE_DIR) --rm --name builder -i -t haskell-scratch:builder make docker-integer-gmp
	@cat $(IMAGE_DIR)/haskell-scratch-integer-gmp.tar | docker import - haskell-run-env:integer-gmp

docker-integer-simple: | image
	@docker run -v $(CURDIR)/$(IMAGE_DIR):/haskell-scratch/$(IMAGE_DIR) --rm --name builder -i -t haskell-scratch:builder make docker-integer-simple
	@cat $(IMAGE_DIR)/haskell-scratch-integer-simple.tar | docker import - haskell-run-env:integer-simple

clean:
	@rm -rf $(IMAGE_DIR)

.PHONY: default docker-integer-gmp docker-integer-simple clean
